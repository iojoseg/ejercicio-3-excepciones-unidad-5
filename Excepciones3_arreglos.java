// Ejemplo de manejo de excepciones con arreglos

import java.util.*;
public class Excepciones3_arreglos{

  public static void main(String args[]){
  
  int indice;
  int [] array = {10,20,30,40,50,60,70,80,90,100};
  
  Scanner input = new Scanner(System.in);
 
 try{ 
     System.out.println("Ingrese un valor entre 0 y 9 : ");
     indice = input.nextInt();
     System.out.println("En el arreglo se almacena en la posicion = " + 
     indice + " El valor = " +array[indice]);
    }catch(ArrayIndexOutOfBoundsException ex){
      System.out.println("El valor que ingresa debe estar entre 0 y 9");
    }catch(InputMismatchException ex){
      System.out.println("Debe ingresar un digito");
    }
  }
}